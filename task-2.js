// Write two ways of creating objects using functions that represent
// the items of the products on the products.jpg on the source directory

// Factory Function
// You code here ...

function orderFood (name, amount, price) {
    return {
        name: name,
        amount: amount,
        price: price,
        orderRelease: function () {
            document.write(`Your Order name is - ${name}, <br> amount: - ${amount}, <br> Total Price: - ${amount * price} sum`);
        }
    };
}
let orderFood2 = orderFood('Burger Mix', '5', '30000');
orderFood2.orderRelease();
let orderFood3 = orderFood('Maxy Petro', '2', '35000');
let orderFood4 = orderFood('Max Traditional', '3', '29000');

// Constructor Function
// You code here ...

function OrderFunc (name, amount, price) {
    this.name = name;
    this.amount = amount;
    this.price = price;
    this.calculateOrder = function () {
        console.log(`Your Order: ${this.name}   Amount: ${this.amount}   Total Price: ${this.amount * this.price} sum`);
    };   
}
const newOrder = new OrderFunc('Mix Box', "5", "30000");
newOrder.calculateOrder();
console.log(newOrder);